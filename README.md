Skeleton WordPress Project
==========================

This project is meant to be used to spin up new projects
quickly that use WordPress. It also allows the use of Vagrant
and other development tools while never touch the WordPress
core code which is a submodule in the `web` directory.

# Installation

    cp composer.dist.json composer.json
    curl -sS https://getcomposer.org/installer | php
    php composer.phar install
    php composer.phar --working-dir="puppet" install
    vagrant up
    ./bin/wp ssh --host=vagrant db create
    ./bin/wp ssh --host=vagrant core config --dbname=wordpress --dbuser=root --dbpass=root
    ./bin/wp ssh --host=vagrant core install --title="Wordpress Vagrant" --admin_user="admin" --admin_password="admin" --admin_email="admin@localhost.local"

Now just open your browser to http://127.0.0.1:8080 and follow the
rest of the WordPress installation.
