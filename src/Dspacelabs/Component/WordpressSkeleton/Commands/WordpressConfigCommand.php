<?php

namespace Dspacelabs\Component\WordpressSkeleton\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

class WordpressConfigCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('wordpress:config')
            ->setDescription('Creates a wp-config.php file')
            ->setDefinition(
                array(
                    new InputOption('db_name', null, InputOption::VALUE_REQUIRED, '', 'wordpress'),
                    new InputOption('db_user', null, InputOption::VALUE_REQUIRED, '', 'root'),
                    new InputOption('db_password', null, InputOption::VALUE_REQUIRED, '', 'root'),
                    new InputOption('db_host', null, InputOption::VALUE_REQUIRED, '', 'localhost'),
                    new InputOption('db_charset', null, InputOption::VALUE_REQUIRED, '', 'utf8'),
                    new InputOption('db_collate', null, InputOption::VALUE_REQUIRED, ''),
                )
            )
            ->setHelp(<<<DOC
DOC
);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $twig = new \Twig_Environment(
            new \Twig_Loader_Filesystem(__DIR__ . '/../Views'),
            array(
                'cache' => false,
            )
        );

        $content = $twig->render(
            'wp-config.php.twig',
            array(
                'db_name'          => $input->getOption('db_name'),
                'db_user'          => $input->getOption('db_user'),
                'db_password'      => $input->getOption('db_password'),
                'db_host'          => $input->getOption('db_host'),
                'db_charset'       => $input->getOption('db_charset'),
                'db_collate'       => $input->getOption('db_collate'),
                'auth_key'         => $this->generateHash(),
                'secure_auth_key'  => $this->generateHash(),
                'logged_in_key'    => $this->generateHash(),
                'nonce_key'        => $this->generateHash(),
                'auth_salt'        => $this->generateHash(),
                'secure_auth_salt' => $this->generateHash(),
                'logged_in_salt'   => $this->generateHash(),
                'nonce_salt'       => $this->generateHash(),
                'table_prefix'     => 'wp_',
                'wplang'           => '',
                'wp_debug'         => 'false',
            )
        );

        $filename = getcwd() . '/web/wp-config.php';
        file_put_contents($filename, $content);
    }

    /**
     * @return string
     */
    private function generateHash()
    {
        return hash_hmac('sha256', microtime(true), uniqid(rand(), true));
    }
}
