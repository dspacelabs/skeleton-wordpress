<?php

namespace Dspacelabs\Component\WordpressSkeleton\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

class WordpressUserListCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('wordpress:user:list')
            ->setDescription('List users in the database')
            ->setDefinition(
                array(
                )
            )
            ->setHelp(<<<DOC
DOC
);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $conn = \Doctrine\DBAL\DriverManager::getConnection(
            array(
                'dbname'   => 'wordpress',
                'user'     => 'root',
                'password' => 'root',
                'host'     => '127.0.0.1',
                'driver'   => 'pdo_mysql',
            ),
            new \Doctrine\DBAL\Configuration()
        );

        $sql    = 'SELECT * FROM wp_users';
        $result = $conn->fetchAll($sql);
        $table  = $this->getHelperSet()->get('table');
        $table
            ->setHeaders(
                array(
                    'id',
                    'user_login',
                    'user_nicename',
                    'user_email',
                    'user_url',
                    'user_registered',
                    'display_name',
                )
            );
        foreach ($result as $user) {
            $table->addRow(
                array(
                    $user['ID'],
                    $user['user_login'],
                    $user['user_nicename'],
                    $user['user_email'],
                    $user['user_url'],
                    $user['user_registered'],
                    $user['display_name'],
                )
            );
        }
        $table->render($output);
    }
}
