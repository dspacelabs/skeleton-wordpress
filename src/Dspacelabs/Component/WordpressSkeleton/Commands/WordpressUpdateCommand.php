<?php

namespace Dspacelabs\Component\WordpressSkeleton\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

class WordpressUpdateCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('wordpress:update')
            ->setDescription('Updates the wordpress submodule')
            ->setDefinition(
                array(
                )
            )
            ->setHelp(<<<DOC
DOC
);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $commandsToRun = array(
            'git submodule foreach git pull origin master',
            'git add web/',
            'git commit -m "Updated WordPress submodule"'
        );

        foreach ($commandsToRun as $command) {
            $process = new Process($command);
            $process->run(function ($type, $buffer) use($output) {
                $output->writeln($buffer);
            });
        }
    }
}
